package com.cliente.apirest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cliente.apirest.models.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {
	
	Cliente findByid(Long id);
	

}
