package com.cliente.apirest.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cliente.apirest.models.Cliente;
import com.cliente.apirest.repository.ClienteRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping(value="/api")
@Api(value="API REST produtos")
@CrossOrigin(origins="*")
public class ClienteResource {

		@Autowired
		ClienteRepository clienteRepository;
		
		@GetMapping("/clientes")
		@ApiOperation(value="Retorna uma lista de clientes")
		public List<Cliente> listProdutos(){
			return 	clienteRepository.findAll();
		}
		
		@GetMapping("/cliente/{id}")
		@ApiOperation(value="Retorna apenas um cliente unico")
		public Cliente listProdutoUnico(@PathVariable(value="id") long id){
			return 	clienteRepository.findByid(id);
		}
		
		@PostMapping("/cliente")
		@ApiOperation(value="Salva um cliente")
		public Cliente salvaProduto(@RequestBody Cliente cliente) {
			return clienteRepository.save(cliente);
		}
		
		@DeleteMapping("/cliente")
		@ApiOperation(value="Deleta um cliente")
		public void deleteProduto(@RequestBody Cliente cliente) {
			clienteRepository.delete(cliente);
		}
		
		@PutMapping("/cliente")
		@ApiOperation(value="Atualiza um cliente")
		public Cliente atualizaProduto(@RequestBody Cliente cliente) {
			return clienteRepository.save(cliente);
		}
	
}
